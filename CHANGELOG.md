# Current Updates

Added New functionality with a focus on HEVC

* Added Recursive fucntionality by default - (not a toggle yet)
* Added 'fastdecode' to tune options by default
* Added temp file transcode by default
* Added delete source, rename temp file and move to source functionality by default

# Previous Updates

* Corrected spelling mistakes
* Added I/O codec logic.  If input codec matches output, `-c:v copy` will be used instead
* Fixed issues with not detecting FPS of incoming video
* Fixed issues with filenames that contain whitespace/spaces
* unRAID Docker https://lime-technology.com/forum/index.php?topic=48798.0 contributed by Bjonness406
* Removed crf_max - not required
* Removed dependance on exiftool - unreliable
* Removed x265params - not required
* Removed unneeded avconv options
* Added ability to choose own filetypes to parse
* Added added echo of current file name in case you want to log the output
* Added sed for parsing FPS instead of exiftool
* Changed output to include errors for troubleshooting.
* Ability to detect input FPS and output at same FPS (default action)
* Ability for user to define FPS for all encoded videos
* Ability to define crf and crf_max (Constant Rate Factor)
* Added "-movflags +faststart" to output move more data to the beginning of the file (decrease stream loading delay)
* Added "-tune film" to output to help preserve the video quality 
* Removed forced FPS predefined in code
* Cleaned up code for *nix compatability
* Support for x265 input and output 
* Support for mp4 container format output
* Support for encoder presets
* Support for audio sample rates
* Added support for copying from h264
* Added support for converting from h263
* Added support for converting from h265
* Moved to GitLab
* Created [Wiki](https://gitlab.com/ThatGuy/convert2mkv/wikis/home)
* Added Home; Future Goals; Requirements; HowTo; Q&A; Supported Codecs & Containers pages
* Copied content from README.md to the Wiki
* Created Issues labels
* Added [LICENSE.md](https://gitlab.com/ThatGuy/convert2mkv/blob/master/LICENSE.md) (GNU AGPL v3.0)
* Added [CHANGELOG.md](https://gitlab.com/ThatGuy/convert2mkv/blob/master/CHANGELOG.md)
* Major rewrite to make script more intelligent
* Script now ignores incoming file extensions completely
* Script now tests for audio and video codecs
* Script now creates an variable string, and passes it on to avconv
* avconv accepts the string and uses it to process the audio and video per codec, as opposed to an all or nothing approach
* Script now determines if OGV files contain datastreams, and maps 0:1 and 0:2 for video and audio respectively
* Corrected the removal of original files after transcoding
* Completely re-written from the ground up, with inspiration from [ChuckPa](https://forums.plex.tv/profile/ChuckPa "ChuckPa") (thanksbuddy!)
* Included more file types (3gp,mpeg,flv,webm,ogv)
* Instead of relying on file extensions, it now uses avprobe to to detect the type of codec used to encode the video
* Transcoding is now done based on the input codec, not on the filetype
* Output can now be adjusted per codec, based on your own preferences
* File ext detection is no longer hindered by upper and lower case characters
* Added forced 25fps for codecs known to have encoding issues with MKV container
* Added forced keyframes for h263 and flv so that the time index is read correctly after h264 conversion
