# Convert2MKV

## Now supporting MP4; h264; and x265/hevc output!

After tireless hours of testing, and many many tankfuls of e-Juice (yes, I'm an ex smoker turned Vaper), a ton of coffee and a heap of input from others, I can now officially say that Convert2MKV now supports

* Output to the MP4 container
* Output to the MKV container
* h264/x264 encoding and decoding
* x265 encoding and decoding

All of which can be used in any combination you choose.

You can have an MP4 file with either x265 or h264 encoded video!  The same for MKV!


## [Issues](https://gitlab.com/ThatGuy/convert2mkv/issues)


## [Did someone say Docker???](https://lime-technology.com/forum/index.php?topic=48798.0) - Thanks to bjønness :)


## [Future Goals](https://gitlab.com/ThatGuy/convert2mkv/milestones)


## [Changelog](https://gitlab.com/ThatGuy/convert2mkv/blob/master/CHANGELOG.md)


## [Requirements](https://gitlab.com/ThatGuy/convert2mkv/wikis/requirements)


## [The Script Itself](https://gitlab.com/ThatGuy/convert2mkv/raw/master/convert2mkv.sh)


## [How To](https://gitlab.com/ThatGuy/convert2mkv/wikis/howto)


## [Q&A](https://gitlab.com/ThatGuy/convert2mkv/wikis/qna)


## [Codec Support](https://gitlab.com/ThatGuy/convert2mkv/wikis/codecs)


## Conclusion

This was originally for my own use, but if there is enough interest in the script, I'll work on it, and make it more flexible.  Good luck, and let me know how it works for you!

Cheers!